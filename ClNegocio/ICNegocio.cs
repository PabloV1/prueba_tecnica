﻿using System.IO;

namespace ICLNegocio
{
     public interface ICNegocio
    {
        void ConectarArchivo(string rutaExtraccion, string rutaGuardadoBovinos, string rutaGuardadoEquinos);
        void SepararTipoAnimal(StreamReader objReader, string rutaGuardadoBovinos, string rutaGuardadoEquinos);
        void ValidarTipoAnimal(char primeraletra, string palabra, string rutaGuardadoBovinos, string rutaGuardadoEquinos);       
    }
}
