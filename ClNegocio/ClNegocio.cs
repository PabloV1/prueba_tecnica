﻿using System.IO;
using ICLNegocio;
using Repositorio;
using ICLRepositorio;

namespace Negocio
{
    public class ClNegocio : ICNegocio
    {
        private readonly ICRepositorio ICRepositorio;

        public ClNegocio(ICRepositorio ICRepositorio)
        {
            this.ICRepositorio = ICRepositorio;
        }

        public void ConectarArchivo(string rutaExtraccion, string rutaGuardadoBovinos, string rutaGuardadoEquinos)
        {
            var objReader = ICRepositorio.LeerArchivo(rutaExtraccion);
            ICRepositorio.ValidarExistencia(rutaGuardadoBovinos);
            ICRepositorio.ValidarExistencia(rutaGuardadoEquinos);
            SepararTipoAnimal(objReader, rutaGuardadoBovinos, rutaGuardadoEquinos);
            objReader.Close();
            ICRepositorio.Mensajes();
            
        }

        public void SepararTipoAnimal(StreamReader objReader, string rutaGuardadoBovinos, string rutaGuardadoEquinos)
        {
            string palabra = " ";

            while (palabra != null)
            {
                palabra = objReader.ReadLine();
                if (palabra != null)
                {
                    char primeraletra = palabra[0];
                    ValidarTipoAnimal(primeraletra, palabra, rutaGuardadoBovinos, rutaGuardadoEquinos);
                }
            }
        }
        public void ValidarTipoAnimal(char primeraletra, string palabra,string rutaGuardadoBovinos, string rutaGuardadoEquinos)
        {
            const char animalConB = 'b';
            var url = (primeraletra == animalConB) ? rutaGuardadoBovinos : rutaGuardadoEquinos;

            ICRepositorio.Guardado(url, palabra);
        }
    }
}
