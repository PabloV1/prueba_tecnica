﻿using System.IO;

namespace ICLRepositorio
{
    public interface ICRepositorio
    {
        StreamReader LeerArchivo(string rutaExtraccion);
        void ValidarExistencia(string url);
        void Guardado(string urltxt, string palabra);
        void Mensajes();
    }
}
