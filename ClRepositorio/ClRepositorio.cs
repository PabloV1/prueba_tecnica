﻿using ICLRepositorio;
using System;
using System.IO;

namespace Repositorio
{
    public class CLRepositorio : ICRepositorio
    {      
        public StreamReader LeerArchivo(string rutaExtraccion)
        {
            StreamReader objReader = new StreamReader(rutaExtraccion);
            return objReader;
        }

        public void ValidarExistencia(string url)
        {
            if (File.Exists(url))
            {
                File.Delete(url);
            }         
        }

        public void Guardado(string urltxt, string palabra)
        {
            try
            {
                StreamWriter objWriter = new StreamWriter(urltxt, true);
                objWriter.WriteLine(palabra);
                objWriter.Close();
                Console.WriteLine("Se ejecuto valido y se guardo correctamente: " + palabra + " en su .txt");
                Console.WriteLine("  ");
            }
            catch (Exception e)
            { 
                Console.WriteLine("Hubo un problema con la ruta: ");
                Console.WriteLine(e.Message);
                Console.WriteLine("  ");
            }        
        }

        public void Mensajes()
        {
            Console.WriteLine("Pulse una letra para salir.....");
            Console.ReadKey();
        }
    }
}
